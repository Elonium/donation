const express = require("express");
const path = require("path")
const bodyparser = require("body-parser");
const ejs = require("ejs");
const session = require("express-session");

let app = express();
       app.use(bodyparser.json());
       app.use(bodyparser.urlencoded({ extended: true }));
       app.engine("html", ejs.renderFile);
       app.set('view engine', 'ejs');
       app.set('views', path.join(__dirname, "./views"));
       app.use(express.static(path.join(__dirname)));
       app.set('trust proxy', true);



       app.get("/", async(req,res) => {
           res.render("index")
       })


       
       app.listen(process.env.PORT || 5000, () => {
           console.log(`App is Online on Port : 5000`)
       })
